Name:    usbutils
Version: 017
Release: 1
Summary: Linux utilities for USB device
License: GPLv2+
URL:     http://www.linux-usb.org/

Source0: https://www.kernel.org/pub/linux/utils/usb/usbutils/%{name}-%{version}.tar.xz
Source1: GPL-2.0.txt
Source2: GPL-3.0.txt


BuildRequires: libusbx-devel systemd-devel gcc autoconf automake libtool
Requires: hwdata

%description
This is a collection of USB tools running on a USB host.

%package        help
Summary:        Including man files for usbutils
Requires:       man

%description    help
This contains man files for the using of usbutils.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -vif
%configure --sbindir=%{_sbindir} --datadir=%{_datadir}/hwdata --disable-usbids
%make_build

%install
%make_install INSTALL="install -p"
install -D -m 644 %{SOURCE1} %{buildroot}%{_defaultlicensedir}/%{name}/GPL-2.0.txt
install -D -m 644 %{SOURCE2} %{buildroot}%{_defaultlicensedir}/%{name}/GPL-3.0.txt
rm -rf %{buildroot}/%{_libdir}/pkgconfig/usbutils.pc

%files
%doc NEWS
%license GPL-2.0.txt GPL-3.0.txt
%{_bindir}/*

%files help
%{_mandir}/*/*

%changelog
* Wed Jan 31 2024 zhanghongtao <zhanghongtao22@huawei.com> - 017-1
- update package to usbutils-017
- lsusb: add fallback names for 'lsusb -v' output
- Honor system libdir and includedir
- usbutils: lsusb-t: print entries for devices with no interfaces
- lsusb -t: sort in bus order, not reverse order
- susb-t: if a driver is not bound to an interface, report "[none]"
- Generate usbutils.pc pkgconfig file
- usbreset: Allow idProduct and idVendor to be 0
- susb: Add function that sorts the output by device ID.
- lsusb: Additional sorting by bus number.
- lsusb: This is a more compact implementation of the device list sort implemented within this pull request. The output remains the same as the one demonstrated in the previous commit.

* Tue Feb 7 2023 zhanghongtao <zhanghongtao22@hiuawei.com> - 015-1
- update package to usbutils-015

* Tue Nov 1 2022 lihaoxiang <lihaoxiang9@huawei.com> - 014-2
- fix an runtime error reported by undefined sanitizer

* Tue Nov 23 2021 yanglongkang <yanglongkang@hiuawei.com> - 014-1
- update package to 014

* Tue Sep 28 2021 Wenchao Hao <haowenchao@huawei.com> - 012-5
- Nothing but to sync master and LTS branch

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 012-3
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Oct 29 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> -012-2
- backport one patch to init string buffer before reading from it

* Thu Jul 16 2020 liubo <liubo254@huawei.com> -012-1
- upgrade package to 012

* Wed Aug 21 2019 zhanghaibo <ted.zhang@huawei.com> - 010-4
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESCi:openEuler Debranding

* Wed Aug 21 2019 Su Weifeng <suweifeng1@huawei.com> - 010-3
- Type:other
- ID:NA
- SUG:NA
- DESC:rename patches
- Package init
